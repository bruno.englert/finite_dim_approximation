# Finite Dim Approximation

Approximating equation F(x) = e^(-x^2/2) with the galerkin and collocation method. 

![Galerkin](galerkin.png)
![Collocation](collocation.png)
