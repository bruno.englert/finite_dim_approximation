import numpy as np
import matplotlib.pyplot as plt


def f(x):
    return np.exp(- x * x / 2)


def l2_norm(x, y):
    return np.mean((y - x) * (y - x))


def get_pol_f_mtx(x, order):
    len = x.shape[0]
    order += 1
    rv = np.zeros((len, order))
    for i in range(order):
        for k in range(len):
            rv[k, i] = np.power(x[k], i)
    return rv


def f_cap(x, coeffs):
    len = x.shape[0]
    rv = np.zeros((len,))
    for k in range(len):
        for i, coeff in enumerate(coeffs):
            rv[k] += np.power(x[k], i) * coeff
    return rv


if __name__ == "__main__":
    order = 10
    collocation_points = order+1
    interval = np.linspace(-3, 3, collocation_points + 1, endpoint=False)[1:]
    phi = get_pol_f_mtx(interval, order=order)
    print("Phi")    
    print(phi)
    phi_inv = np.linalg.inv(phi)
    interval = interval.reshape((collocation_points,1))
    q = np.linalg.solve(phi, f(interval))
    print("Q")    
    print(q)

    x_interval = np.linspace(-3, 3, 1000, endpoint=False)[1:]
    err = l2_norm(f_cap(x_interval,q), f(x_interval))
    print("Error")
    print(err)

    plt.plot(x_interval, f(x_interval), label='original')
    plt.plot(x_interval, f_cap(x_interval, q), label='aproximation')
    plt.legend(loc='upper left')
    plt.show()
