import scipy
import scipy.integrate as integrate
import scipy.special as special
import numpy as np
import matplotlib.pyplot as plt


def f(x):
    return np.exp(- x * x / 2)


def l2_norm(x, y):
    return np.mean((y - x) * (y - x))


def get_q(order, interval):
    order += 1
    rv = np.zeros((order,))
    for i in range(order):
        rv[i] = integrate.quad(lambda x: np.power(x, i) * f(x), interval[0], interval[1])[0]
    return rv


def phi_l2(order, interval):
    order += 1
    rv = np.zeros((order, order))
    for i in range(order):
        for k in range(order):
            rv[i, k] = integrate.quad(lambda x: np.power(x, i) * np.power(x, k), interval[0], interval[1])[0]
    return rv


def f_cap(x, coeffs):
    len = x.shape[0]
    rv = np.zeros((len,))
    for k in range(len):
        for i, coeff in enumerate(coeffs):
            rv[k] += np.power(x[k], i) * coeff
    return rv


if __name__ == "__main__":
    order = 5
    interval = (-3, 3)
    q = get_q(order, interval)
    q = np.linalg.solve(phi_l2(order, interval), q)
    x_interval = np.linspace(-3, 3, 1000, endpoint=False)[1:]
    err = l2_norm(f_cap(x_interval, q), f(x_interval))
    print(err)

    plt.plot(x_interval, f(x_interval), label='original')
    plt.plot(x_interval, f_cap(x_interval, q), label='aproximation')
    plt.legend(loc='upper left')
    plt.show()
